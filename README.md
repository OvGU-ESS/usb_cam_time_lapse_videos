# USB-Cam Time-Lapse 

A python-script take every x seconds a picture from a connected webcam. Afterwards the pictures will merged into a short clip.

## The Script
(Current v0.1.0)

The script place the taken images into a new folder. At the top of the script the
name of the new time lapse images and the time between two images could be edit.

The command `python run.py` executes a generic configuration. Take a view to  
 `python run.py --help` in order get information about the optional parameters.

Configuration examples:
* `python run.py --state r --size 1 --pause 3 --name myProject` - records a 640x680 image every third second and save it in /myProject
* `python run.py --state a --size 1` - visualizes the current view of the camera with an resolution of 640x680 
* `python run.py --state d ` - detects all available devices 

### History

#### v0.2.0

The script can be parametrized now related to the camera, resolution, project name, and period.

#### v0.1.0

This Scripts takes every 10s a picture from a connected usb-web-cam and write it in the folder of the script.

## Create Clip

With mencoder you can use this commandline inside the folder with the pictures from the script.

```
mencoder mf://*.jpg -mf fps=15:type=jpg -ovc lavc \-lavcopts vcodec=mpeg4:mbd=2:trell:vbitrate=8000  -oac copy -o output.avi
```