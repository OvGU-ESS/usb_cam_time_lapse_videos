#Author: Sebastian Zug
#Author: Martin Seidel
#Version: 0.2.0
import cv2
import cv2.cv as cv
import numpy
import time
import datetime
import os
import signal
import argparse
import sys

default_name = 'myTimeLapseProject'
default_pause=1

def signal_handler(signal, frame):
  print('Aus Maus!')
  sys.exit(0)

def printCameraInfo(camera):
  return "%4.0f x %4.0f"  % (camera.get(cv.CV_CAP_PROP_FRAME_WIDTH), 
                             camera.get(cv.CV_CAP_PROP_FRAME_HEIGHT))

def runCameraDetection():
  print "Checking opencv devices"
  cameras = []
  for i in range(0,3):
    temp_camera = cv2.VideoCapture(i)
    if(temp_camera.isOpened()):
      print "Camera " + str(i) + " running with " + printCameraInfo(temp_camera)
      del(temp_camera)
      cameras.append(i)
    else:
      print "Camera " + str(i) + " not available with "

def getCamera(camID, width, height):
  print "Adjust selected camera"
  signal.signal(signal.SIGINT, signal_handler)
  cap = cv2.VideoCapture(camID)
  cap.set(cv.CV_CAP_PROP_FRAME_WIDTH, width) 
  cap.set(cv.CV_CAP_PROP_FRAME_HEIGHT, height) 
  return cap

def runCameraAdjustment(camID, width, height):
  cap = getCamera(camID, width, height)
  while 1:
    r, frame = cap.read()
    cv2.imshow('Smile!', frame)
    cv2.waitKey(1)

def recordImages(camID, width, height, pause, projectName):
  directory = './'+ projectName + '_' + time.strftime("%Y-%m-%d") +'/';
  os.makedirs(directory)
  
  cap = getCamera(camID, width, height)
  i=0
  time_old=0
  while 1:
    r, frame = cap.read()
    if time.time()-time_old > pause:
      time_old = time.time()
      current_time=time.strftime("%Y-%m-%d_%H-%M-%S")
      cv2.imwrite(directory + "/" + current_time + "-" + str(datetime.datetime.now().microsecond) + "_" + projectName + ".jpg", frame)
      cv2.imshow('Last recorded image', frame)
      print "image nr %7d taken at "%i + current_time
      cv2.waitKey(1)
      i = i+1

      
if __name__ == "__main__":
 
  parser = argparse.ArgumentParser(description='Python script for generating time lapse movies.')
  parser.add_argument('--pause', dest='pause', type=int, default=default_pause,
                    help='time interval [s]')
  parser.add_argument('--state', dest='state', default='r',
                    help='States = "r" record, "a" camera adjustment, "d" camera detection')
  parser.add_argument('--camID', dest='camID', default=-1,
                    help='Camera ID')
  parser.add_argument('--size', dest='size', type = int, default=0,
                    help='States = "0" 640x480, "1" 1280x720, "2" ..')
  parser.add_argument('--name', dest='name', default=default_name,
                    help='Project name (folder)')

  args = parser.parse_args()
 
  print "-------------------------------------------------------------- "
  print "Record time lapse movies"
  print "-------------------------------------------------------------- "
 
  if args.size == 0:
    width = 640
    height = 480
  elif args.size == 1:
    width = 1280
    height = 720
  else:
    print "Resolution not defined!"
    signal_handler(None, None)
  print "Choosen resolution %d x %d" %(width, height)
  
  if args.state == 'd':
    runCameraDetection()
  elif args.state == 'a':
    runCameraAdjustment(args.camID, width, height)
  elif args.state == 'r':
    recordImages(args.camID, width, height, args.pause, args.name)